#!/usr/bin/env python3

#####################################################
#### Kiosk Project. Tono Riesco Oct. 2015-2019  #####
#####################################################

# Import the card driver
from smartcard.System import readers

# Import the firefox driver with the keys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Firefox()

# Get the config for the site
from site_config import *

import binascii
import time
import urllib.request, urllib.error, urllib.parse
import os

DEBUG = False
contador = 0
card_detected = 0

# Start Firefox with the main page
driver.get(URL_CENTRAL)

# Check if the page is open
#assert "Kiosk" in driver.title

# Wait for 5 seconds to be sure that firefox has stated
#time.sleep(3)

# Put firefox in fullscreen
#elem.send_keys(Keys.F11)

# browser full window
driver.fullscreen_window()

while True:

  try:
    r = readers()
    if DEBUG:
      print("PCSC readers:", r)
    reader = r[0]
    if DEBUG:
      print("Using reader:", reader)
    card = reader.createConnection()
    card.connect()
    #card = context.connect(reader)
    # The call Card.reconnect() resets the card to a known state. Not always required, but good practice.

    #card.reconnect()

    data, sw1, sw2 = card.transmit(COMMAND)
    if DEBUG:
      print("Data received: ", data)
      #print "Command: %02X %02X" % (sw1, sw2)
    ### We have to reverse the number since is in Hexa but first is last.. and convert to hex
    data_reversed = data[::-1]
    ## Since the dosimeter return 8 bytes, we have to erase the last 3 that are for checksum
    if len(data_reversed) == 8:
      del data_reversed[0:2]

    if DEBUG:
        print("Number Reversed: ", number_reversed)

    number_string = binascii.hexlify(bytearray(data_reversed))

    if DEBUG:
      print("Number HEX: ", number_string)

    number_int = int(number_string , 16)
    if DEBUG:
      print(str(number_int))

    ### Compose the URL
    #url = 'http://sam2.cern.ch/site.php?number=' + str(number_int) 
    #url = 'Location: https://apex.cern.ch/pls/htmldb_devdb11/f?p=ADAMS4AP:AP:::::RFID,ACCESS_POINT:' + str(number_int) + ',' + SITE
    # For normal use with access point:
    #url = 'https://oraweb.cern.ch/pls/adams/adams_web.AP_ACCESS?p_number=' + str(number_int) + '&p_access_point=' + SITE + '&p_photo=Y'
    
    # For PROD
    ##url = 'http://apex.cern.ch/pls/htmldb_edmsdb/f?p=273:700:::::P700_PRS_ID,P700_ACP_CODE:' + str(number_int) + ',' + SITE

    # For DEV
    #url = 'http://apex.cern.ch/pls/htmldb_devdb11/f?p=273:700:::::P700_PRS_ID,P700_ACP_CODE:' + str(number_int) + ',' + SITE

    # For XMAS 2016
    # url = 'http://oraweb.cern.ch/ords/edmsdb/adams3/api/situationatacp/' + str(number_int) + '/' + SITE

    # For RP Kiosk. The last /Y is for showing the Impacts
    url = 'http://oraweb.cern.ch/ords/edmsdb/adams3/api/situationatacp/' + str(number_int) + '/' + SITE + '/Y'
    # For DEV
    #url = 'http://apex.cern.ch/pls/htmldb_devdb11/f?p=273:700:::::P700_PRS_ID,P700_ACP_CODE:' + str(number_int) + ',' + SITE
    ## For Xmas Break:
    #url = 'Location: https://oraweb.cern.ch/pls/adams/adams_web.ZONE_ACCESS?p_number=' + str(number_int) + '&p_ZONE=' + SITE + '&p_photo=Y'
    if DEBUG:
      print(url)
  
    card.disconnect()

    del card
    #del context

    #### Play a sound ####

    os.system("/usr/bin/aplay -q /opt/sound/sound.wav")

    #### Reload browser with index page
    driver.get(url)

    contador = 0
    # White a bit to avoid 2 or 3 reads if the user doesn't take out the card. All the work has been done, we have to wait for the ADAMS page...so, no problem
    time.sleep(2)
    card_detected = 1
  except: 
    # Time waiting
    time.sleep(CYCLE)
    contador+=CYCLE

    if contador > TIME_WAIT and card_detected == 1:
      driver.get(URL_CENTRAL)
      contador = 0
      card_detected = 0

