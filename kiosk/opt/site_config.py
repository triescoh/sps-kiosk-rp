### COMMAND to read card Mifare
COMMAND = [ 0xFF, 0xCA, 0x00, 0x00, 0x00 ]

WORKING_DIR = "/opt"
FILE_SITE = "/opt/site"

# First and default screen
URL_CENTRAL = "http://ssm.cern.ch/sam/slideshow-sps.html"
# Time, in seconds, waiting the information before going to the default screen
TIME_WAIT = 20
# CYCLE defines how long we wait until we check the reader for a new card in seconds
CYCLE = 0.1
DEBUG = False
# The site ADAMS to be check the authorisation
SITE = "YDA01=BA80"
