#!/bin/bash
git update-index --assume-unchanged "zabbix-env"
git update-index --assume-unchanged "kiosk/opt/site_config.py"

git pull
git add .
git reset -- zabbix-env
git commit -a -m "Kiosk RP  updated"
git push
