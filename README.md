# Introduction
Kiosk Project for the SPS Access Points.

# Installation
* Since the update to Python3 you need to install update the host as follow:
```bash
apt install python3-pyscard
apt install python3-selenium
```
* The kiosk/opt directory files must be copied to */* in the host. Otherways, change the kiosk.py script paths. The final path for all files must be: /opt/kiosk.py
* The kiosk/autostart directory must be copied to /home/kiosk/.config/ to start the kiosk, the xhost patch to allow the remote keypress and to start the sound card.
* The watchtower docker must be rebuild with:   
```docker-compose up -d watchtower```   
with the environment variable PASS pointing the pass for gmail if you want to receive notifications about the update of containers. Doesn't work with the SMTP at CERN :rage:
```bash
root@kiosk.host# export PASS=xxxxx
root@kiosk.host# docker-compose up -d watchtower
```
* You should receive a mail with the first run of watchtower, if not, check what is going on with:
```bash
root@kiosk.host# docker logs watchdog
```
* The __syspref.js__ file must be saved to: /etc/firefox/ changing the hostname with the local hostname. 
* The __site__ file must be filled with the ADAMS authorisation needed in the kiosk access point, i.e. **YDA01=BA80**
* /etc/hostname must be edited to get the new hostname. The best is running in /etc and /root the following command:
!! Avoid modify the hidden files! Specially .git ... otherwise the git is broken!
```bash
find ./ -type f  -not -path '*/\.*' | xargs sed -i 's/newhost/oldhost/g'

find ./ -type f  -not -path '*/\.*' | xargs sed -i 's/ydaip01/ydaip01/g'
```
