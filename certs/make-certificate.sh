openssl genrsa -out `hostname`.key 1024
touch openssl.cnf
cat >> openssl.cnf <<EOF
[ req ]
prompt = no
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
C = CH
ST = Geneva
L = Geneva
O = CERN
OU = BE
CN = `hostname`.cern.ch
emailAddress = root@`hostname`.cern.ch
EOF
openssl req -config openssl.cnf -new -key `hostname`.key -out `hostname`.csr
openssl x509 -req -days 3650 -in `hostname`.csr -signkey `hostname`.key -out `hostname`.crt
