docker run -d -p 9000:9000 \
--name portainer \
--restart always \
-v /root/docker/certs:/certs \
-v /var/run/docker.sock:/var/run/docker.sock \
-v portainer_data:/data \
sps-registry.cern.ch:5000/portainer \
--ssl \
--sslcert /certs/`hostname`.crt \
--sslkey /certs/`hostname`.key
